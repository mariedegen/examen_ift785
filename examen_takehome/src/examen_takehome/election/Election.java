package examen_takehome.election;

import examen_takehome.Log;
import examen_takehome.bulletin.Bulletin;
import examen_takehome.electeur.Electeur;

import java.util.*;

/**
 * Classe abstraite servant de base commune à divers type d'éléctions
 */
public abstract class Election extends Observable
{
    // Votes.
    private Vector<Bulletin>[] votes;

    // Queue des electeurs.
    private Queue<Electeur> electeurs;

    // Liste des examen_takehome.medias
    private List medias;

    /*
        Constructeur par défaut
     */
    public Election() {
        electeurs = new LinkedList<Electeur>();
        medias = new ArrayList();
    }

    /*
        Accesseur des votes
        @return votes
     */
    public Vector<Bulletin>[] getVotes()
    {
        return votes;
    }

    /*
        Accesseur des votes avec un index spécifique
        @param i : votes avec cette valeur
        @return votes[i]
     */
    public Vector<Bulletin> getVotes(int i)
    {
        return votes[i];
    }

    /*
        Mutateur des votes
        @param votes : Vector<Bulletin>
     */
    protected void setVotes(Vector<Bulletin>[] votes)
    {
        this.votes = votes;
    }

    /*
        Mutateur des votes avec un indice défini
        @param votes : Vector<Bulletin>
        @param i : indice
    */
    protected void setVotes(int i, Vector<Bulletin> v)
    {
        votes[i] = v;
    }

    /*
        Méthode du prochain electeur dans la queue
        @return electeur
     */
    public Electeur getElecteurSuivant ()
    {
        return electeurs.poll();
    }

    /*
        Ajout d'un electeur dans la queue
        @param e : Electeur
     */
    public void addElecteurQueue(Electeur e)
    {
        electeurs.add(e);
    }

    /*
        Methode de vérification si la queue est vide
        @return true si non vide false sinon
     */
    public boolean hasElecteur()
    {
        return !electeurs.isEmpty();
    }

    /*
        Méthodes abstraites des différents etats
     */
    public abstract void ouvrir();
    public abstract void fermer();
    public abstract void votation();
    public abstract Object depouiller();

    /*
        Méthodes abstraites pour la gestion de l'electeur
     */
    public abstract boolean estEligible(Electeur e);
    protected abstract Bulletin createBulletin();
    protected abstract void enregistrerVote(Bulletin b);

    /*
        Methode isoloir, permet a l'electeur de suivre un meme processus de vote
        @param e : Electeur
     */
    public final void isoloir(Electeur e)
    {
        if (estEligible(e))
        {
            Bulletin b = createBulletin();
            e.voter(b);

            if (b.isValide())
                enregistrerVote(b);
            else
                Log.error("Vote invalide : " + b.getVoteIndex());
        }
    }

}

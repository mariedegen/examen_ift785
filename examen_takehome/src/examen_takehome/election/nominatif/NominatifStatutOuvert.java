package examen_takehome.election.nominatif;
import examen_takehome.electeur.Electeur;

/**
 * Classe pour le statut ouvert pour le scrutin nominatif
 */
public class NominatifStatutOuvert extends NominatifStatut {

    /*
        Constructeur
     */
    public NominatifStatutOuvert(Nominatif nom) {
        super(nom);
    }

    /*
        Methode votation spécifique pour les nominatifs
     */
    @Override
    public void votation()
    {
        Electeur unElecteur = getNominatif().getElecteurSuivant();
        getNominatif().isoloir(unElecteur);
    }

    /*
        Methode fermer spécifique pour les nominatifs
     */
    @Override
    public void fermer()
    {

    }

    /*
        @return l'état suivant
     */
    @Override
    public NominatifStatut suivant()
    {
        return new NominatifStatutTermine(getNominatif());
    }
}

package examen_takehome.election.nominatif;

import examen_takehome.Log;
import examen_takehome.bulletin.BulletinNominatif;

import java.util.Arrays;

/**
 * Classe pour le statut termine pour le scrutin nominatif
 */
public class NominatifStatutTermine extends NominatifStatut {

    /*
        Constructeur
     */
    public NominatifStatutTermine(Nominatif nom) {
        super(nom);
    }

    /*
        Methode depouiller spécifique pour les nominatifs
     */
    @Override
    public Object depouiller()
    {
        int nbCAQ = getNominatif().getVotes(BulletinNominatif.CAQ).size();
        int nbPLQ = getNominatif().getVotes(BulletinNominatif.PLQ).size();
        int nbPQ = getNominatif().getVotes(BulletinNominatif.PQ).size();
        int nbQS = getNominatif().getVotes(BulletinNominatif.QS).size();

        int[] array = {nbCAQ, nbPLQ, nbPQ, nbQS};
        int max = Arrays.stream(array).max().getAsInt();

        if(max == nbCAQ)
            return BulletinNominatif.CAQ;
        else if(max == nbPLQ)
            return BulletinNominatif.PLQ;
        else if(max == nbPQ)
            return BulletinNominatif.PQ;
        else if(max == nbQS)
            return BulletinNominatif.QS;
        else
            return null;
    }

    /*
        @return si demande retourne null car pas d'etat suivant et affichage d'un log
     */
    @Override
    public NominatifStatut suivant()
    {
        Log.log("Pas de suivant, l'état terminé est l'état final pour les votes de type nominatif");
        return null;
    }
}

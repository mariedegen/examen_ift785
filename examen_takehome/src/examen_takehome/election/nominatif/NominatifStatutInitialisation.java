package examen_takehome.election.nominatif;

/**
 * Classe pour le statut d'initialisation pour le scrutin Nominatif
 */
public class NominatifStatutInitialisation extends NominatifStatut{

    /*
        Constructeur
     */
    public NominatifStatutInitialisation(Nominatif nom) {
        super(nom);
    }

    /*
        Methode ouvrir spécifique pour les scrutins nominatifs
        Ne fais rien actuellement
     */
    @Override
    public void ouvrir()
    {

    }

    /*
        @return l'état suivant
     */
    @Override
    public NominatifStatut suivant()
    {
        return new NominatifStatutOuvert(getNominatif());
    }
}

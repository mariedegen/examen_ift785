package examen_takehome.election.nominatif;

import examen_takehome.election.Election;
import examen_takehome.bulletin.Bulletin;
import examen_takehome.bulletin.BulletinNominatif;
import examen_takehome.electeur.Electeur;
import examen_takehome.electeur.ElecteurNominatif;

import java.util.Vector;

/**
    Classe pour les scrutins Nominatif
 */
public class Nominatif extends Election {

    // Etat du vote nominatif.
    private NominatifStatut etat;

    /*
        Constructeur par defaut
     */
    public Nominatif()
    {
        etat = new NominatifStatutInitialisation(this);

        setVotes(new Vector[4]);
        setVotes(BulletinNominatif.CAQ, new Vector<Bulletin>());
        setVotes(BulletinNominatif.PLQ, new Vector<Bulletin>());
        setVotes(BulletinNominatif.PQ, new Vector<Bulletin>());
        setVotes(BulletinNominatif.QS, new Vector<Bulletin>());
    }

    /*
        Implementation de la méthode abstraite
        Methode d'ouverture de l'election
     */
    public void ouvrir()
    {
        etat.ouvrir();
        etat = etat.suivant();

        setChanged();
        notifyObservers(new String("open"));
    }

    /*
        Implementation de la méthode abstraite
        Methode de la fermeture de l'election
    */
    public void fermer()
    {
        etat.fermer();
        etat = etat.suivant();

        depouiller();

        setChanged();
        notifyObservers(new String("close"));
    }

    /*
        Implementation de la méthode abstraite
        Methode de votation de l'election
    */
    public void votation ()
    {

        while(hasElecteur()){
            etat.votation();
            setChanged();
            notifyObservers(new String("vote"));
        }
    }

    /*
        Implementation de la méthode abstraite
        Methode de dépouille de l'election
        @return le vainqueur de l'élection
    */
    public Object depouiller ()
    {
        return etat.depouiller();
    }


    /*
        Implementation de la méthode abstraite
        Methode de l'eligibilite pour le referendum
        @param e : Electeur
        @return true si eligible false sinon
    */
    public boolean estEligible(Electeur e)
    {
        ElecteurNominatif en = (ElecteurNominatif)e;

        if(e.getAge() >= 18 && e.getAge() <= 50 &&
                en.getS() == ElecteurNominatif.SEXE.FEMME && !e.isdejaVote())
        {
            return true;
        }
        return false;
    }

    /*
        Méthode de creation d'un bulletion specifique au type de vote
        @return BulletinNominatif
    */
    protected Bulletin createBulletin()
    {
        return new BulletinNominatif();
    }

    /*
        Enregistre le vote
        @param b : bulletin
    */
    protected void enregistrerVote(Bulletin b)
    {
        getVotes(b.getVoteIndex()).add(b);
    }
}

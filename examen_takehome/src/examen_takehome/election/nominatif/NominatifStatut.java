package examen_takehome.election.nominatif;
import examen_takehome.Log;

/**
 *  Classe de gestion du statut des elections nominatives
 */
public class NominatifStatut {

    //Objet Nominatif
    private Nominatif nominatif;

    /*
        Constructeur
        @param nom : obj nominatif
     */
    public NominatifStatut(Nominatif nom)
    {
        this.nominatif = nom;
    }

    /*
        Comportement par defaut de la fonction ouvrir
     */
    public void ouvrir()
    {
        Log.error("Statut nominatif pour ouvrir invalide");
    }

    /*
        Comportement par defaut de la fonction fermer
     */
    public void fermer()
    {
        Log.error("Statut nominatif pour fermer invalide");
    }

    /*
        Comportement par defaut de la fonction votation
     */
    public void votation()
    {
        Log.error("Statut nominatif pour votation invalide");
    }

    /*
        Comportement par defaut de la fonction depouiller
     */
    public Object depouiller()
    {
        Log.error("Statut nominatif pour dépouiller invalide");
        return null;
    }

    /*
        @return etat suivant (initialisation -> ouvert -> termine)
     */
    public NominatifStatut suivant()
    {
        return null;
    }

    /*
        @return obj nominatif
     */
    public Nominatif getNominatif()
    {
        return nominatif;
    }
}

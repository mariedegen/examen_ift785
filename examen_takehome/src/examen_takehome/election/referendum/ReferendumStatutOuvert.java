package examen_takehome.election.referendum;
import examen_takehome.electeur.Electeur;

/**
 * Classe pour le statut ouvert pour le scrutin Referendum
 */
public class ReferendumStatutOuvert extends ReferendumStatut
{
    /*
        Constructeur
     */
    public ReferendumStatutOuvert(Referendum ref)
    {
        super(ref);
    }

    /*
        Methode votation spécifique pour les referendums
     */
    @Override
    public void votation()
    {
        Electeur unElecteur = getReferendum().getElecteurSuivant();
        getReferendum().isoloir(unElecteur);
    }

    /*
        Methode fermer spécifique pour les referendums
     */
    @Override
    public void fermer()
    {

    }

    /*
        @return l'état suivant
     */
    @Override
    public ReferendumStatut suivant()
    {
        return new ReferendumStatutTermine(getReferendum());
    }
}

package examen_takehome.election.referendum;
import examen_takehome.Log;
import examen_takehome.bulletin.BulletinReferendum;

/**
 * Classe pour le statut termine pour le scrutin referendum
 */
public class ReferendumStatutTermine extends ReferendumStatut
{

    /*
        Constructeur
     */
    public ReferendumStatutTermine(Referendum ref)
    {
        super(ref);
    }

    /*
        Methode depouiller spécifique pour les referundums
     */
    @Override
    public Object depouiller()
    {
        int nbOui = getReferendum().getVotes(BulletinReferendum.OUI).size();
        int nbNon = getReferendum().getVotes(BulletinReferendum.NON).size();

        if (nbOui > nbNon)
            return BulletinReferendum.OUI;
        else if (nbOui < nbNon)
            return BulletinReferendum.NON;
        else
            return null;
    }

    /*
        @return si demande retourne null car pas d'etat suivant et affichage d'un log
     */
    @Override
    public ReferendumStatut suivant()
    {
        Log.log("Pas de suivant, l'état terminé est l'état final pour les votes de type référendum");
        return null;
    }
}

package examen_takehome.election.referendum;

/**
 * Classe pour le statut d'initialisation pour le scrutin Referendum
 */
public class ReferendumStatutInitialisation extends ReferendumStatut
{
    /*
        Constructeur
     */
    public ReferendumStatutInitialisation(Referendum ref) {
        super(ref);
    }

    /*
        Methode ouvrir spécifique pour les referendum
        Ne fais rien actuellement
     */
    @Override
    public void ouvrir()
    {

    }

    /*
        @return l'état suivant
     */
    @Override
    public ReferendumStatut suivant()
    {
        return new ReferendumStatutOuvert(getReferendum());
    }
}

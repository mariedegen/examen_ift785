package examen_takehome.election.referendum;

import examen_takehome.election.Election;
import examen_takehome.bulletin.Bulletin;
import examen_takehome.bulletin.BulletinReferendum;
import examen_takehome.electeur.Electeur;

import java.util.Vector;

/**
 Classe pour les scrutins Referendum
 */
public class Referendum extends Election
{
    // Etat du refendum.
    private ReferendumStatut etat;

    /*
        Constructeur par defaut
     */
    public Referendum()
    {
        etat = new ReferendumStatutInitialisation(this);

        setVotes(new Vector[2]);
        setVotes(BulletinReferendum.OUI, new Vector<Bulletin>());
        setVotes(BulletinReferendum.NON, new Vector<Bulletin>());
    }

    /*
        Implementation de la méthode abstraite
        Methode d'ouverture de l'election
     */
    public void ouvrir()
    {
        etat.ouvrir();
        etat = etat.suivant();

        setChanged();
        notifyObservers(new String("open"));
    }

    /*
        Implementation de la méthode abstraite
        Methode de la fermeture de l'election
    */
    public void fermer()
    {
        etat.fermer();
        etat = etat.suivant();
        depouiller();

        setChanged();
        notifyObservers(new String("close"));
    }

    /*
        Implementation de la méthode abstraite
        Methode de votation de l'election
    */
    public void votation ()
    {

        while(hasElecteur()){
            etat.votation();
            setChanged();
            notifyObservers(new String("vote"));
        }
    }

    /*
        Implementation de la méthode abstraite
        Methode de dépouille de l'election
        @return le vainqueur de l'élection
    */
    public Object depouiller ()
    {
        return etat.depouiller();
    }

    /*
        Implementation de la méthode abstraite
        Methode de l'eligibilite pour le referendum
        @param e : Electeur
        @return true si eligible false sinon
    */
    public boolean estEligible(Electeur e)
    {
        if(e.getAge() >= 18 && !e.isdejaVote())
        {
            return true;
        }
        return false;
    }

    /*
        Méthode de creation d'un bulletion specifique au type de vote
        @return BulletinReferendum
     */
    protected Bulletin createBulletin()
    {
        return new BulletinReferendum();
    }

    /*
        Enregistre le vote
        @param b : bulletin
     */
    protected void enregistrerVote(Bulletin b)
    {
        getVotes(b.getVoteIndex()).add(b);
    }
}

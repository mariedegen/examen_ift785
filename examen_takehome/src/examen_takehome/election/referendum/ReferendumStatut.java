package examen_takehome.election.referendum;
import examen_takehome.Log;

/**
 *  Classe de gestion du statut des elections referendum
 */
public class ReferendumStatut
{
    //Objet Referendum
    private Referendum referendum;

    /*
        Constructeur
        @param ref : obj referendum
     */
    public ReferendumStatut(Referendum ref)
    {
        this.referendum = ref;
    }

    /*
        Comportement par defaut de la fonction ouvrir
     */
    public void ouvrir()
    {
        Log.error("Statut référendum pour ouvrir invalide");
    }

    /*
        Comportement par defaut de la fonction fermer
     */
    public void fermer()
    {
        Log.error("Statut référendum pour fermer invalide");
    }

    /*
        Comportement par defaut de la fonction votation
     */
    public void votation()
    {
        Log.error("Statut référendum pour votation invalide");
    }

    /*
        Comportement par defaut de la fonction depouiller
     */
    public Object depouiller()
    {
        Log.error("Statut référendum pour dépouiller invalide");
        return null;
    }

    /*
        @return etat suivant (initialisation -> ouvert -> termine)
     */
    public ReferendumStatut suivant()
    {
        return null;
    }

    /*
        @return obj referendum
     */
    public Referendum getReferendum()
    {
        return referendum;
    }
}

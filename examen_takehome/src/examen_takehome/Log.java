package examen_takehome;

/**
 * Classe Log
 */
public class Log {

    /*
        Methode qui affiche le message de log
     */
    static public void log(String message)
    {
        System.out.println(message);
    }

    /*
        Methode qui lance une exception avec le message.
    */
    static public void error(String message) throws RuntimeException
    {
        System.err.println(message);
        throw new RuntimeException(message);
    }
}

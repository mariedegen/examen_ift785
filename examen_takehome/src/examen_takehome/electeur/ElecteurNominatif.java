package examen_takehome.electeur;
import examen_takehome.bulletin.Bulletin;

/**
 * Classe ElecteurNominatif spécialisée de la classe abstraite Electeur
 */

public class ElecteurNominatif extends Electeur {

    /*
        Attribut spécifique a l'electeur nominatif
     */
    private SEXE s;

    /*
        ENUM SEXE : HOMME ou FEMME
     */
    public enum SEXE{
        FEMME,
        HOMME
    }

    /*
        Constructeur
     */
    public ElecteurNominatif(int age, SEXE s)
    {
        super(age);
        this.s = s;
    }

    /*
        Accesseur de l'attribut sexe
        @return s : sexe
     */
    public SEXE getS() {
        return s;
    }

    /*
        Méthode de vote mettant en place une stratégie de vote
        @param b : bulletin
    */
    public void voter(Bulletin b) {
        if(getAge() >= 18 && getAge() <= 29){
            b.voter(0);
        }else if(getAge() > 29 && getAge() <= 35){
            b.voter(1);
        }else if(getAge() > 35 && getAge() <= 40){
            b.voter(2);
        }else{
            b.voter(3);
        }
    }
}

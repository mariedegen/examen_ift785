package examen_takehome.electeur;
import examen_takehome.bulletin.Bulletin;

/**
 * Classe ElecteurReferendum spécialisée de la classe abstraite Electeur
 */

public class ElecteurReferendum extends Electeur
{

    /*
        Constructeur
     */
    public ElecteurReferendum(int age)
    {
        super(age);
    }

    /*
        Méthode de vote mettant en place une stratégie de vote
        @param b : bulletin
     */
    public void voter(Bulletin b)
    {
        if(getAge() > 40)
        {
            b.voter(0);
        }
        else
        {
            b.voter(1);
        }
    }

}

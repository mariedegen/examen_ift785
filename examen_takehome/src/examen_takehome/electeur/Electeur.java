package examen_takehome.electeur;
import examen_takehome.bulletin.Bulletin;

/**
 * Classe abstraite Electeur servant de base commune à divers types de Electeurs
 */
public abstract class Electeur
{

    /*
        Attributs
        - age : int
        - dejaVote : boolean
     */
    private int age;
    private boolean dejaVote;

    /*
        Constructeur
        @param age
     */
    public Electeur(int age)
    {
        this.age = age;
        this.dejaVote = false;
    }

    /*
        Methode abstraite voter
        Methode specifique a chaque type d'electeur
     */
    public abstract void voter(Bulletin b);

    /*
        Accesseur de l'age
        @return age
     */
    public int getAge()
    {

        return age;
    }

    /*
        Methode accesseur de la valeur de dejaVote
        @return dejaVote
     */
    public boolean isdejaVote()
    {
        return dejaVote;
    }

    /*
        Mutateur de dejaVote
        @param dejaVote
     */
    public void setdejaVote(boolean dejaVote)
    {
        this.dejaVote = dejaVote;
    }

}

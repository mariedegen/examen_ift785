package examen_takehome;

import examen_takehome.electeur.ElecteurNominatif;
import examen_takehome.electeur.ElecteurReferendum;
import examen_takehome.election.nominatif.Nominatif;
import examen_takehome.election.referendum.Referendum;
import examen_takehome.medias.ObserverLaPresse;
import examen_takehome.medias.ObserverRadioCanada;

public class Main
{
    public static void main(String[] args)
    {
        // Referendum
        System.out.println("\n ----------------     Referendum     ------------------ \n");
        Referendum ref = new Referendum();

        ref.addObserver(new ObserverRadioCanada());
        ref.addObserver(new ObserverLaPresse());

        ref.addElecteurQueue(new ElecteurReferendum(16));
        ref.addElecteurQueue(new ElecteurReferendum(45));
        ref.addElecteurQueue(new ElecteurReferendum(25));
        ref.addElecteurQueue(new ElecteurReferendum(54));

        ref.ouvrir();
        ref.votation();
        ref.fermer();

        System.out.println("Résultat du référendum : " + ref.depouiller());
        System.out.println("\n ------------------------------------------------------- \n");

        // Nominatif
        System.out.println("\n ----------------       Nominatif      ------------------ \n");
        Nominatif nom = new Nominatif();

        nom.addObserver(new ObserverRadioCanada());
        nom.addObserver(new ObserverLaPresse());

        nom.addElecteurQueue(new ElecteurNominatif(18, ElecteurNominatif.SEXE.FEMME));
        nom.addElecteurQueue(new ElecteurNominatif(45, ElecteurNominatif.SEXE.HOMME));
        nom.addElecteurQueue(new ElecteurNominatif(25, ElecteurNominatif.SEXE.FEMME));
        nom.addElecteurQueue(new ElecteurNominatif(54, ElecteurNominatif.SEXE.FEMME));
        nom.addElecteurQueue(new ElecteurNominatif(30, ElecteurNominatif.SEXE.FEMME));
        nom.addElecteurQueue(new ElecteurNominatif(35, ElecteurNominatif.SEXE.FEMME));
        nom.addElecteurQueue(new ElecteurNominatif(40, ElecteurNominatif.SEXE.FEMME));
        nom.addElecteurQueue(new ElecteurNominatif(45, ElecteurNominatif.SEXE.FEMME));
        nom.addElecteurQueue(new ElecteurNominatif(30, ElecteurNominatif.SEXE.FEMME));
        nom.addElecteurQueue(new ElecteurNominatif(31, ElecteurNominatif.SEXE.FEMME));
        nom.addElecteurQueue(new ElecteurNominatif(32, ElecteurNominatif.SEXE.FEMME));
        nom.addElecteurQueue(new ElecteurNominatif(33, ElecteurNominatif.SEXE.FEMME));

        nom.ouvrir();
        nom.votation();
        nom.fermer();

        System.out.println("Résultat du vote nominatif : " + nom.depouiller());

    }
}

package examen_takehome.bulletin;

/**
 * Classe BulletinNominatif spécialisée de la classe abstraite Bulletin
 */
public class BulletinNominatif extends Bulletin {

    /*
        Attributs specifiques a la classe
     */
    public final static int CAQ = 0;
    public final static int PQ = 1;
    public final static int PLQ = 2;
    public final static int QS = 3;

    /*
        Methode qui verifie si le vote est valide
        @return true si oui sinon false
     */
    public boolean isValide()
    {
        return getVoteIndex() == CAQ || getVoteIndex() == PQ
                || getVoteIndex() == PLQ || getVoteIndex() == QS;
    }

}

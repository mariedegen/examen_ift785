package examen_takehome.bulletin;

/**
 * Classe BulletinReferendum spécialisée de la classe abstraite Bulletin
 */
public class BulletinReferendum extends Bulletin {

    /*
        Attributs specifiques a la classe, OUI/NON
    */
    public final static int OUI = 0;
    public final static int NON = 1;

    /*
        Methode qui verifie si le vote est valide
        @return true si oui sinon false
     */
    public boolean isValide()
    {
        return getVoteIndex() == OUI || getVoteIndex() == NON;
    }
}
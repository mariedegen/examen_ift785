package examen_takehome.bulletin;

/**
 * Classe abstraite Bulletin servant de base commune à divers types de Bulletin
 */
public abstract class Bulletin {

    /*
        Attribut vote
     */
    private int vote;

    /*
        Méthode permettant le vote
        @param vote
     */
    public void voter(int vote)
    {
        this.vote = vote;
    }

    /*
        Récupérer le vote
        @return vote
     */
    public int getVoteIndex()
    {
        return vote;
    }

    /*
        Abstract methode
        @return true si vote valide false sinon
     */
    public abstract boolean isValide();
}

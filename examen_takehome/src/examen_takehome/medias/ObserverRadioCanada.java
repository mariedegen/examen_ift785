package examen_takehome.medias;
import java.util.Observable;
import java.util.Observer;

/**
 * Classe ObserverRadioCanada
 */
public class ObserverRadioCanada extends RadioCanada implements Observer {

    /*
        Methode de mise à jour du media en fonction de l'election
        @obj : Observable -> Election
        @arg : ce qui a changé
     */
    public void update(Observable obj, Object arg)
    {
        String msg = (String)arg;
        switch (msg)
        {
            case "open":
                debutElection();
                break;
            case "vote":
                unNouveauVote();
                break;
            case "close":
                finElection();
                break;
        }
    }
}

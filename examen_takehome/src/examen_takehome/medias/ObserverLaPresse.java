package examen_takehome.medias;
import java.util.Observable;
import java.util.Observer;

/**
 * Classe ObserverRadioCanada
 */
public class ObserverLaPresse extends LaPresse implements Observer {

    /*
        Methode de mise à jour du media en fonction de l'election
        @obj : Observable -> Election
        @arg : ce qui a change
    */
    public void update(Observable obj, Object arg) {
        String msg = (String) arg;
        switch (msg) {
            case "open":
                ouverteElection();
                break;
            case "vote":
                nouveauVote();
                break;
            case "close":
                fermetureElection();
                break;
        }
    }
}

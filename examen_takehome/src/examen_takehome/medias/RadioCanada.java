package examen_takehome.medias;
import examen_takehome.Log;

/**
    Classe unique RadioCanada
 */
public class RadioCanada
{
    /*
        Methode getInstance
        @return l'instance
     */
    synchronized static public RadioCanada getInstance()
    {
        return null;
    }

    /*
        Méthode affichant un log quand l'election debute
     */
    public void debutElection()
    {
        Log.log("RadioCanada open");
    }

    /*
        Methode affichant un log quand il y a un nouveau vote
     */
    public void unNouveauVote()
    {
        Log.log("RadioCanada vote");
    }

    /*
        Methode affichant un log quand l'election est fini
     */
    public void finElection()
    {
        Log.log("RadioCanada end");
    }
}

package examen_takehome.medias;
import examen_takehome.Log;

/**
 Classe unique LaPresse
 */
public class LaPresse
{
    /*
        Methode getInstance
        @return l'instance
    */
    synchronized static public LaPresse getInstance()
    {

        return null;
    }

    /*
        Méthode affichant un log quand l'election debute
    */
    public void ouverteElection()
    {
        Log.log("LaPresse open");
    }

    /*
        Methode affichant un log quand il y a un nouveau vote
    */
    public void nouveauVote()
    {
        Log.log("LaPresse vote");
    }

    /*
        Methode affichant un log quand l'election est fini
    */
    public void fermetureElection()
    {
        Log.log("LaPresse end");
    }
}

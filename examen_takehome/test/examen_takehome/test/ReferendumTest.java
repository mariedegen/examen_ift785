package examen_takehome.test;

import examen_takehome.bulletin.Bulletin;
import examen_takehome.bulletin.BulletinReferendum;
import examen_takehome.electeur.ElecteurNominatif;
import examen_takehome.electeur.ElecteurReferendum;
import examen_takehome.election.referendum.Referendum;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Vector;

import static org.junit.jupiter.api.Assertions.*;

public class ReferendumTest
{
    private Referendum referendum;

    @BeforeEach
    void setUp()
    {
        referendum = new Referendum();
    }

    @AfterEach
    void tearDown()
    {
        referendum = null;
    }

    @Test
    public void ouvrir()
    {
        assertDoesNotThrow(() -> referendum.ouvrir(), "Exception inatendue lors de l'appel a ouvrir().");
    }

    @Test
    public void fermer()
    {
        assertThrows(RuntimeException.class, () -> referendum.fermer(), "Exception attendue lors d'un appel a fermer() dans le mauvais etat.");
        referendum.ouvrir();

        assertDoesNotThrow(() -> referendum.fermer(), "Exception inatendue lors de l'appel a fermer().");
    }

    @Test
    public void votation()
    {
        // On a besoin d'au moins un electeur sinon la boucle interne de la votation ne fait rien.
        referendum.addElecteurQueue(new ElecteurReferendum(24));

        assertThrows(RuntimeException.class, () -> referendum.votation(), "Exception attendue lors d'un appel a votation() dans le mauvais etat.");
        referendum.ouvrir();

        assertDoesNotThrow(() -> referendum.votation(), "Exception inatendue lors de l'appel a votation().");
    }

    @Test
    public void depouiller()
    {
        referendum.ouvrir();

        referendum.addElecteurQueue(new ElecteurReferendum(24));
        referendum.votation();

        assertThrows(RuntimeException.class, () -> referendum.depouiller(), "Exception attendue lors d'un appel a depouiller() dans le mauvais etat.");

        referendum.fermer();

        assertDoesNotThrow(() -> referendum.depouiller(), "Exception inatendue lors de l'appel a depouiller().");
        assertNotNull(referendum.depouiller(), "Depouiller a retourne null.");
    }

    @Test
    public void estEligible()
    {
        // Eligibles.
        assertTrue(referendum.estEligible(new ElecteurReferendum(Integer.MAX_VALUE)));
        assertTrue(referendum.estEligible(new ElecteurReferendum(24)));
        assertTrue(referendum.estEligible(new ElecteurReferendum(18)));

        assertTrue(referendum.estEligible(new ElecteurNominatif(19, ElecteurNominatif.SEXE.FEMME)));

        // Non eligible.
        assertFalse(referendum.estEligible(new ElecteurReferendum(Integer.MIN_VALUE)));
        assertFalse(referendum.estEligible(new ElecteurReferendum(16)));
        assertFalse(referendum.estEligible(new ElecteurReferendum(-1)));

        assertFalse(referendum.estEligible(new ElecteurNominatif(16, ElecteurNominatif.SEXE.FEMME)));
    }

    @Test
    public void electeursNominatifs()
    {
        referendum.ouvrir();

        referendum.addElecteurQueue(new ElecteurNominatif(36, ElecteurNominatif.SEXE.FEMME)); // Va voter 2

        assertThrows(RuntimeException.class, () -> referendum.votation(), "Exception attendue pour un bulletin invalide.");

        referendum.fermer();
    }

    @Test
    public void electeurNonEligible()
    {
        referendum.ouvrir();

        referendum.addElecteurQueue(new ElecteurReferendum(16));
        referendum.votation();

        referendum.fermer();
        assertNull(referendum.depouiller(), "Null attendu.");
    }

    @Test
    public void getVotes()
    {
        referendum.ouvrir();
        // On fait voter 3 eligibles et 2 non eligibles.
        referendum.addElecteurQueue(new ElecteurReferendum(25));
        referendum.addElecteurQueue(new ElecteurReferendum(27));
        referendum.addElecteurQueue(new ElecteurReferendum(35));

        referendum.addElecteurQueue(new ElecteurReferendum(14));
        referendum.addElecteurQueue(new ElecteurReferendum(8));

        referendum.votation();
        referendum.fermer();

        // On doit obtenir 3 votes.
        int nbVotes = 0;

        for (Vector<Bulletin> v : referendum.getVotes())
            nbVotes += v.size();

        assertEquals(3, nbVotes, "Le nombre de votes ne correspond pas au nombre d'electeurs eligibles.");
    }

    @Test
    public void getVotesParType()
    {
        referendum.ouvrir();
        // On fait voter 3 eligibles et 2 non eligibles.
        referendum.addElecteurQueue(new ElecteurReferendum(25));
        referendum.addElecteurQueue(new ElecteurReferendum(27));
        referendum.addElecteurQueue(new ElecteurReferendum(45));
        referendum.addElecteurQueue(new ElecteurReferendum(52));

        referendum.votation();
        referendum.fermer();

        // On doit obtenir 2 votes pour 0.
        assertEquals(2, referendum.getVotes(0).size());

        // On doit obtenir 2 votes pour 1.
        assertEquals(2, referendum.getVotes(1).size());
    }

    @Test
    public void hasElecteur()
    {
        assertFalse(referendum.hasElecteur());
        referendum.addElecteurQueue(new ElecteurReferendum(24));
        assertTrue(referendum.hasElecteur(), "");
        assertTrue(referendum.hasElecteur(), "Appeler hasElecteur() deux fois de suite a change le resultat."); // On s'assure qu'appeler la fonction plusieurs fois ne retire pas les electeurs.
    }

    @Test
    public void getElecteurSuivant()
    {
        assertNull(referendum.getElecteurSuivant(), "Un electeur a ete retourne alors qu'aucun n'etait dans la queue.");

        ElecteurReferendum a = new ElecteurReferendum(19);
        ElecteurReferendum b = new ElecteurReferendum(22);

        referendum.addElecteurQueue(a);
        referendum.addElecteurQueue(b);

        assertEquals(a, referendum.getElecteurSuivant(), "Un electeur inconnu a ete retourne.");
        assertEquals(b, referendum.getElecteurSuivant(), "Un electeur inconnu a ete retourne.");
        assertNull(referendum.getElecteurSuivant(), "Le nombre d'electeurs retournes est plus grand que le nombre d'electeurs ajoutes.");
    }
}
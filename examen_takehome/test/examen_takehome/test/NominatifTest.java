package examen_takehome.test;

import examen_takehome.electeur.ElecteurNominatif;
import examen_takehome.election.nominatif.Nominatif;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NominatifTest {

    private Nominatif nominatif;

    @BeforeEach
    void setUp() {
        nominatif = new Nominatif();
    }

    @AfterEach
    void tearDown() {
        nominatif = null;
    }

    @Test
    void ouvrir() {
        assertDoesNotThrow(() -> nominatif.ouvrir(), "Exception inatendue lors de l'appel a ouvrir().");
    }

    @Test
    void fermer() {
        assertThrows(RuntimeException.class, () -> nominatif.fermer(), "Exception attendue lors d'un appel a fermer() dans le mauvais etat.");
        nominatif.ouvrir();

        assertDoesNotThrow(() -> nominatif.fermer(), "Exception inatendue lors de l'appel a fermer().");
    }

    @Test
    void votation() {
        // On a besoin d'au moins un electeur sinon la boucle interne de la votation ne fait rien.
        nominatif.addElecteurQueue(new ElecteurNominatif(24, ElecteurNominatif.SEXE.FEMME));

        assertThrows(RuntimeException.class, () -> nominatif.votation(), "Exception attendue lors d'un appel a votation() dans le mauvais etat.");
        nominatif.ouvrir();

        assertDoesNotThrow(() -> nominatif.votation(), "Exception inatendue lors de l'appel a votation().");
    }

    @Test
    void depouiller() {
        nominatif.ouvrir();

        nominatif.addElecteurQueue(new ElecteurNominatif(24, ElecteurNominatif.SEXE.FEMME));
        nominatif.votation();

        assertThrows(RuntimeException.class, () -> nominatif.depouiller(), "Exception attendue lors d'un appel a depouiller() dans le mauvais etat.");

        nominatif.fermer();

        assertDoesNotThrow(() -> nominatif.depouiller(), "Exception inatendue lors de l'appel a depouiller().");
        assertNotNull(nominatif.depouiller(), "Depouiller a retourne null.");
    }

    @Test
    void estEligible() {
        // Eligibles.
        assertTrue(nominatif.estEligible(new ElecteurNominatif(30, ElecteurNominatif.SEXE.FEMME)));
        assertTrue(nominatif.estEligible(new ElecteurNominatif(40, ElecteurNominatif.SEXE.FEMME)));
        assertTrue(nominatif.estEligible(new ElecteurNominatif(25, ElecteurNominatif.SEXE.FEMME)));
        assertTrue(nominatif.estEligible(new ElecteurNominatif(19, ElecteurNominatif.SEXE.FEMME)));

        // Non eligible.
        assertFalse(nominatif.estEligible(new ElecteurNominatif(19, ElecteurNominatif.SEXE.HOMME)));
        assertFalse(nominatif.estEligible(new ElecteurNominatif(17, ElecteurNominatif.SEXE.FEMME)));
        assertFalse(nominatif.estEligible(new ElecteurNominatif(54, ElecteurNominatif.SEXE.FEMME)));
        assertFalse(nominatif.estEligible(new ElecteurNominatif(16, ElecteurNominatif.SEXE.FEMME)));
    }
}